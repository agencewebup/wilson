<?php

namespace wilson;

class Auth
{
    public $loginRedirect;
    /** null == allow all */
    public $allow;
    protected $sessionKey = 'auth';

    /**
     * Récupere les données de l'utilisateurs courrant
     * @return [type] [description]
     */
    public function user($key)
    {
        return $_SESSION[$this->sessionKey][$key];
    }

    /**
     * login
     */
    public function login($user = true)
    {
        $_SESSION[$this->sessionKey] = $user;
        session_regenerate_id();
    }

    /**
     * logout
     */
    public function logout()
    {
        unset($_SESSION[$this->sessionKey]);
        session_regenerate_id();
    }

    /**
     * @return boolean true si l'utilisateur est loggé
     */
    public function isLogged()
    {
        return isset($_SESSION[$this->sessionKey]);
    }

    /**
     * @return boolean true si l'utilisateur peut acceder à un controller
     */
    public function isAuthorized($controller)
    {
        return $this->allow == null || in_array($controller, $this->allow) || $this->isLogged();
    }

    public function setSessionKey($key)
    {
        $this->sessionKey = $key;
    }
}
