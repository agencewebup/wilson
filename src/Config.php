<?php

namespace wilson;

/**
 * @deprecated Cette classe sera supprimée dans la pochaine version
 */
class Config
{
    public static $config;

    public static function parseConfig($filePath)
    {
        $json = file_get_contents($filePath);
        self::$config = self::jsonCleanDecode($json, true);

        if (json_last_error() != JSON_ERROR_NONE) {
            die('config.json invalide');
        }

        // clean path
        if (array_key_exists('path', self::$config)) {
            foreach (self::$config['path'] as $key => $value) {
                self::$config['path'][$key] = realpath($value);
            }
        }
    }

    public static function get($key)
    {
        $path = explode('.', $key);

        $array = self::$config;
        foreach ($path as $k) {
            if (array_key_exists($k, $array)) {
                $array = $array[$k];
            } else {
                $array = null;
                break;
            }
        }

        return $array;
    }

    public static function set($key, $value)
    {
        $path = explode('.', $key);
        $array =& self::$config;
        foreach ($path as $k) {
            if (! array_key_exists($k, $array)) {
                $array[$k] = array();
            }
            $array =& $array[$k];
        }

        $array = $value;
    }

    /**
     * @todo bouger ca dans une classe utilities
     */
    public static function jsonCleanDecode($json, $assoc = false, $depth = 512, $options = 0)
    {
        // search and remove comments like /* */ and //
        $json = preg_replace("#(/\*([^*]|[\r\n]|(\*+([^*/]|[\r\n])))*\*+/)|([\s\t](//).*)#", '', $json);

        if (version_compare(phpversion(), '5.4.0', '>=')) {
            $json = json_decode($json, $assoc, $depth, $options);

        } elseif (version_compare(phpversion(), '5.3.0', '>=')) {
            $json = json_decode($json, $assoc, $depth);

        } else {
            $json = json_decode($json, $assoc);
        }

        return $json;
    }
}
