<?php

namespace wilson;

abstract class Controller
{
    protected $format = 'html';
    protected $layout = 'default';
    protected $response;
    protected $action;
    protected $template;
    protected $Auth;
    protected $view;
    protected $alertClass;
    public $router;

    /**
     * Constructeur
     * @param [type] $action [description]
     * @param array  $args [description]
     */
    public function __construct($action, $args = array(), $response, $router)
    {
        $this->args = $args;
        $this->action = $action;
        $this->template = $action;
        $this->router = $router;

        $this->Auth = new Auth();
        $this->view = new views\View();

        $this->response = $response;

        $this->alertClass = array(
            'success' => 'js-alert success',
            'error' => 'js-alert error',
            'warning' => 'js-alert warning',
            'info' => 'js-alert info',
        );

        $this->init();

        // check page public ou que l'utilisateur est loggé
        if ($this->Auth->isAuthorized(get_called_class())) {
            $this->$action();
            $this->beforeRender();

        // redirection vers la page login
        } else {
            if ($this->Auth->loginRedirect) {
                $this->redirect($this->Auth->loginRedirect);
            } else {
                throw new ForbiddenException();
            }
        }
    }

    /**
     * Comportement général par defaut
     */
    protected function init()
    {
    }

    /**
     * Appeller avant le rendu
     */
    protected function beforeRender()
    {
        $this->initAlert();
    }

    public function json($json)
    {
        $this->format = 'json';

        $this->response->contentType = 'application/json';
        $this->response->body = json_encode($json);
    }

    /**
     * Genere le rendu de la page
     * @return string html
     */
    public function render()
    {
        if ($this->format != 'json') {
            $this->response->body = $this->view->render();
        }

        return $this->response;
    }

    /**
     * Reverse url
     * @param  string|array $route string url absolu | array reverse url call Router::link
     * @param  bool prefixe l'url de host
     * @return string url
     */
    public function url($url, $fullbase = false)
    {
        return $this->router->url($url, $fullbase);
    }

    /**
     * Redirige vers une page
     * @param  string|array $route string url absolu | array reverse url call Router::link
     */
    public function redirect($route, $fullbase = false, $status = 302)
    {
        $url = $this::url($route, $fullbase);

        $this->response->status = $status;
        $this->response->header('Location', $url);

        // @todo trouver une meilleur solution que le send exit
        $this->response->send();
        exit;
    }

    public function translate($locale, $localePath, $domain = 'default')
    {
        putenv('LC_ALL=' . $locale);
        setlocale(LC_ALL, $locale);

        bindtextdomain($domain, $localePath);
        bind_textdomain_codeset($domain, 'UTF-8');
        textdomain($domain);
    }

    public static function className()
    {
        $classname = get_called_class();

        if ($pos = strrpos($classname, '\\')) {
            $classname = substr($classname, $pos + 1);
        }

        return $classname;
    }

    public function isRobot($userAgent)
    {
        $robots = array(
            'Google',
            'msnbot',
            'Rambler',
            'Yahoo',
            'AbachoBOT',
            'accoona',
            'AcoiRobot',
            'ASPSeek',
            'CrocCrawler',
            'Dumbot',
            'FAST-WebCrawler',
            'GeonaBot',
            'Gigabot',
            'Lycos',
            'MSRBOT',
            'Scooter',
            'AltaVista',
            'IDBot',
            'eStyle',
            'Scrubby',
        );

        foreach ($robots as $robot) {
            if (stristr($userAgent, $robot)) {
                return true;
            }
        }

        return false;
    }

    public function setAlert($message, $type)
    {
        $class = '';

        if (array_key_exists($type, $this->alertClass)) {
            $class = $this->alertClass[$type];

        } else {
            trigger_error("le type d'alerte '{$type}' n'existe pas.");
        }

        $_SESSION['alert'] = array(
            'message' => $message,
            'class' => $class
        );
    }

    public function initAlert()
    {
        $alert = '';

        if (isset($_SESSION['alert'])) {
            $class = $_SESSION['alert']['class'];
            $message = $_SESSION['alert']['message'];

            $alert = '<div class="' . $class . '">' . $message . '</div>';

            unset($_SESSION['alert']);
        }

        $this->view->set('alert', $alert);
    }
}
