<?php

class DebugBar
{
    private static $errors = array();
    private static $database = array();
    private static $seo = array('title' => '', 'description' => '');

    /**
     * Affiche la debug bar
     */
    public static function display()
    {
        $error = error_get_last();

        if ($error) {
            self::collect($error['type'], $error['message'], $error['file'], $error['line'], null);
        }

        require_once 'debugbar.tpl';
    }

    public static function collect($errno, $errstr, $errfile, $errline, $errcontext)
    {
        self::$errors[] = array(
            'errno' => $errno,
            'errstr' => $errstr,
            'errfile' => $errfile,
            'errline' => $errline,
            'errcontext' => $errcontext
        );
    }

    public static function request($sql)
    {
        self::$database[] = $sql;
    }

    public static function seo($key, $value)
    {
        self::$seo[$key] = $value;
    }
}
