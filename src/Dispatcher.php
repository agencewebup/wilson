<?php

namespace wilson;

/**
 * Le Dispatcher s'occupe de charger le bon Controller pour une requête donnée
 *
 * @package wilson
 */
class Dispatcher
{
    /**
     * Router
     * @var Router
     */
    private $router;

    /**
     * Constructeur
     * @param Router $router
     */
    public function __construct($router)
    {
        $this->router = $router;
    }

    /**
     * Charge le controller pour une URL donnée
     * @param  Request $request
     */
    public function dispatch($request)
    {
        $route = $this->router->route($request->urlPath());
        $this->router->setBaseUrl($request->baseUrl());
        $this->router->setPrefix($route['prefix']);
        $this->router->setLang($route['lang']);
        $this->router->setCurrentRoute($route);

        try {
            $this->invoke($route, $request);

        } catch (\Exception $exception) {
            $controller = $this->router->exceptionHandler();
            if ($controller) {
                $route['controller'] = $this->router->exceptionHandler();
                $route['action'] = 'init';
                $route['args']['exception'] = $exception;
                $this->invoke($route, $request);
            } else {
                trigger_error("Aucun exception handler n'est définit", E_USER_ERROR);
            }
        }
    }

    /**
     * Instancie le controller et l'invoque
     * @param array   $route   route contenant les informations du controlleur à instancier
     * @param Request $request
     */
    private function invoke($route, $request)
    {
        if (isset($route['controller'])) {
            $response = new \wilson\Response();
            $controller = new $route['controller']($route['action'], $route['args'], $response, $this->router);
            $response = $controller->render();
            $response->send();
        } else {
            throw new \wilson\NotFoundException();
        }
    }
}
