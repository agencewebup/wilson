<?php

namespace wilson;

class Form
{
    protected $values = array();
    protected $errors = array();
    protected $rules = array();

    const ERROR_CLASS = 'f-error';
    const ERROR_MESSAGE_CLASS = 'f-error-message';
    const REQUIRED_CLASS = 'f-required';

    public function __construct()
    {
        $this->bind($_POST);
    }

    public function bind($data)
    {
        $this->values = array_merge($data, $this->values);
    }

    public function open($attributes = array())
    {
        if (! array_key_exists('method', $attributes)) {
            $attributes['method'] = 'POST';
        }
        $html = '<form' . $this->attributes($attributes) . '>';

        return $html;
    }

    public function close()
    {
        $_SESSION['csrf']['token'] = uniqid();
        $html = $this->hidden('csrf', $_SESSION['csrf']['token']);
        $html .= '</form>';
        return $html;
    }


    public function openFieldset($attributes = array())
    {
        $html = '<fieldset' . $this->attributes($attributes) . '>';
        return $html;
    }

    public function closeFieldset()
    {
        $html = '</fieldset>';
        return $html;
    }

    public function legend($title)
    {
        return $this->tag('legend', array(), $title);
    }

    public function text($name, $label = false, $attributes = array())
    {
        return $this->input('text', $name, $label, $attributes);
    }

    public function password($name, $label = false, $attributes = array())
    {
        return $this->input('password', $name, $label, $attributes);
    }

    public function email($name, $label = false, $attributes = array())
    {
        return $this->input('email', $name, $label, $attributes);
    }

    public function number($name, $label = false, $attributes = array())
    {
        return $this->input('number', $name, $label, $attributes);
    }

    public function file($name, $label, $attributes = array())
    {
        return $this->input('file', $name, $label, $attributes);
    }

    public function textarea($name, $label = false, $attributes = array())
    {
        $field = '';
        $divClass = '';
        if (array_key_exists('divClass', $attributes)) {
            $divClass = $attributes['divClass'];
            unset($attributes['divClass']);
        }

        $ruleAttributes = $this->getRuleAttributes($name);

        // label
        if ($label) {
            if (array_key_exists('required', $ruleAttributes)) {
                $label .= ' '. $this->tag('span', array('class' => static::REQUIRED_CLASS), '*');
            }
            $field = $this->tag('label', array('for'=>$name), $label);
        }

        // field
        $attrs = array(
            'name' => $name,
            'id' => $name,
        );
        $attrs = array_merge($attrs, $attributes, $ruleAttributes);
        $value = '';
        if (array_key_exists($name, $this->values)) {
            $value = $this->values[$name];
        }
        $field .= $this->tag('textarea', $attrs, $value);

        // errors
        if (array_key_exists($name, $this->errors)) {
            foreach ($this->errors[$name] as $message) {
                $field .= $this->tag('span', array('class'=>static::ERROR_MESSAGE_CLASS), $message);
            }

            $divClass .= ' ' . static::ERROR_CLASS;
        }

        $divAttrs = array();
        if ($divClass) {
            $divAttrs['class'] = $divClass;
        }

        return $this->tag('div', $divAttrs, $field);
    }

    public function checkbox($name, $label = false, $attributes = array())
    {
        $divClass = '';
        if (array_key_exists('divClass', $attributes)) {
            $divClass = $attributes['divClass'];
            unset($attributes['divClass']);
        }

        $ruleAttributes = $this->getRuleAttributes($name);

        // field
        $attrs = array(
            'type' => 'checkbox',
            'name' => $name,
            'id' => $name,
            'value' => 1
        );
        if (array_key_exists($name, $this->values) && $this->values[$name]) {
            $attrs['checked'] = true;
        }
        $attrs = array_merge($attrs, $attributes, $ruleAttributes);
        $field = $this->hidden($name, 0);
        $field .= $this->tag('input', $attrs);

        // label
        if ($label) {
            if (array_key_exists('required', $ruleAttributes) || array_key_exists('required', $attributes)) {
                unset($attributes['required']);
                $label .= ' '. $this->tag('span', array('class' => static::REQUIRED_CLASS), '*');
            }
            $field .= $this->tag('label', array('for'=>$name), $label);
        }

        // errors
        if (array_key_exists($name, $this->errors)) {
            foreach ($this->errors[$name] as $message) {
                $field .= $this->tag('span', array('class'=>static::ERROR_MESSAGE_CLASS), $message);
            }

            $divClass .= ' ' . static::ERROR_CLASS;
        }

        $divAttrs = array();
        if ($divClass) {
            $divAttrs['class'] = $divClass;
        }

        return $this->tag('div', $divAttrs, $field);
    }

    public function radio($name, $label = false, $options = array(), $attributes = array())
    {
        $divClass = '';
        if (array_key_exists('divClass', $attributes)) {
            $divClass = $attributes['divClass'];
            unset($attributes['divClass']);
        }

        $ruleAttributes = $this->getRuleAttributes($name);
        if (array_key_exists('required', $ruleAttributes) || array_key_exists('required', $attributes)) {
            unset($attributes['required']);
            $label .= ' '. $this->tag('span', array('class' => static::REQUIRED_CLASS), '*');
        }
        $spanClass = '';
        if (array_key_exists('spanClass', $attributes)) {
            $spanClass = $attributes['spanClass'];
            unset($attributes['spanClass']);
        }
        if ($spanClass) {
            $field = $this->tag('span', array('class' => $spanClass), $label);
        } else {
            $field = $this->tag('span', array(), $label);
        }

        $currentValue = null;
        if (array_key_exists($name, $this->values)) {
            $currentValue = $this->values[$name];
        }

        foreach ($options as $value => $label) {
            $attrs = array(
                'type' => 'radio',
                'name' => $name,
                'id' => $name.'-'.$value,
                'value' => $value
            );
            if ($currentValue == $value) {
                $attrs['checked'] = true;
            }
            $field .= $this->tag('input', $attrs);
            $field .= $this->tag('label', array('for'=>$name.'-'.$value), $label);
        }

        // errors
        if (array_key_exists($name, $this->errors)) {
            foreach ($this->errors[$name] as $message) {
                $field .= $this->tag('span', array('class'=>static::ERROR_MESSAGE_CLASS), $message);
            }

            $divClass .=  ' ' . static::ERROR_CLASS;
        }

        $divAttrs = array();
        if ($divClass) {
            $divAttrs['class'] = $divClass;
        }

        return $this->tag('div', $divAttrs, $field);
    }

    public function hidden($name, $value, $attributes = array())
    {
        return $this->tag('input', array_merge(array('type' => 'hidden', 'name' => $name, 'value' => $value), $attributes));
    }

    public function select($name, $label = false, $options = array(), $attributes = array())
    {
        $field = '';
        $divClass = '';
        if (array_key_exists('divClass', $attributes)) {
            $divClass = $attributes['divClass'];
            unset($attributes['divClass']);
        }

        $class = '';
        if (array_key_exists('class', $attributes)) {
            $class = $attributes['class'];
            unset($attributes['class']);
        }

        $ruleAttributes = $this->getRuleAttributes($name);

        // label
        if ($label) {
            if (array_key_exists('required', $ruleAttributes) || array_key_exists('required', $attributes)) {
                unset($attributes['required']);
                $label .= ' '. $this->tag('span', array('class' => static::REQUIRED_CLASS), '*');
            }
            $field = $this->tag('label', array('for'=>$name), $label);
        }

        // options
        $currentValue = null;
        if (array_key_exists($name, $this->values)) {
            $currentValue = $this->values[$name];
        }

        $opts = '';
        foreach ($options as $value => $label) {
            if ($currentValue == $value) {
                $opts .= '<option value="'.$value.'" selected>'.$label.'</option>';
            } else {
                $opts .= '<option value="'.$value.'">'.$label.'</option>';
            }
        }

        // select
        $attrs = array(
            'name' => $name,
            'id' => $name,
            'class' => $class
        );
        $attrs = array_merge($attrs, $attributes, $ruleAttributes);

        $field .= $this->tag('select', $attrs, $opts);

        // errors
        if (array_key_exists($name, $this->errors)) {
            foreach ($this->errors[$name] as $message) {
                $field .= $this->tag('span', array('class'=>static::ERROR_MESSAGE_CLASS), $message);
            }

            $divClass .=  ' ' . static::ERROR_CLASS;
        }

        $divAttrs = array();
        if ($divClass) {
            $divAttrs['class'] = $divClass;
        }

        return $this->tag('div', $divAttrs, $field);
    }

    public function button($text, $attributes = array())
    {
        $attrs = $this->attributes($attributes);

        $html = '<button '. $attrs .'>' . $text . '</button>';
        return $html;
    }


    public function input($type, $name, $label = false, $attributes = array())
    {
        $field = '';
        $divClass = '';
        if (array_key_exists('divClass', $attributes)) {
            $divClass = $attributes['divClass'];
            unset($attributes['divClass']);
        }

        $ruleAttributes = $this->getRuleAttributes($name);

        // label
        if ($label) {
            if (array_key_exists('required', $ruleAttributes) || array_key_exists('required', $attributes)) {
                unset($attributes['required']);
                $label .= ' '. $this->tag('span', array('class' => static::REQUIRED_CLASS), '*');
            }
            $field = $this->tag('label', array('for'=>$name), $label);
        }

        // field
        $attrs = array(
            'type' => $type,
            'name' => $name,
            'id' => $name,
        );
        if (array_key_exists($name, $this->values) && $type != 'file') {
            $attrs['value'] = $this->values[$name];
        }
        $attrs = array_merge($attrs, $attributes, $ruleAttributes);
        $field .= $this->tag('input', $attrs);

        // errors
        if (array_key_exists($name, $this->errors)) {
            foreach ($this->errors[$name] as $message) {
                $field .= $this->tag('span', array('class'=>static::ERROR_MESSAGE_CLASS), $message);
            }

            $divClass .= ' ' . static::ERROR_CLASS;
        }

        $divAttrs = array();
        if ($divClass) {
            $divAttrs['class'] = $divClass;
        }

        return $this->tag('div', $divAttrs, $field);
    }

    public function tag($name, $attributes = array(), $content = null)
    {
        $html = '';

        // balise orpheline
        if (in_array($name, array('input'))) {
            $html = '<'. $name . $this->attributes($attributes) .'>';
        } else {
            $html = '<'. $name . $this->attributes($attributes) .'>' . $content . '</' . $name . '>';
        }

        return $html;
    }

    protected function attributes($attributes)
    {
        $attrs = ' ';

        foreach ($attributes as $key => $value) {
            if ($key == 'required') {
                $attrs .= 'required ';
            } elseif ($key == 'checked') {
                $attrs .= 'checked ';
            } elseif ($key == 'selected') {
                $attrs .= 'selected ';
            } else {
                $attrs .= $key . '="' . $value . '" ';
            }
        }

        return $attrs;
    }

    protected function getRuleAttributes($name)
    {
        $attributes = array();

        if (array_key_exists($name, $this->rules) && in_array('required', $this->rules[$name])) {
            $attributes['required'] = true;
        }

        return $attributes;
    }

    public function validate($rules = null)
    {
        if ($rules === null) {
            $rules = $this->rules;
        }
        $ok = false;

        if (isset($_POST['csrf'])) {
            $ok = true;

            foreach ($rules as $name => $ruleArray) {
                foreach ($ruleArray as $key => $rule) {
                    $value = null;

                    if (array_key_exists($name, $this->values)) {
                        $value = $this->values[$name];
                    }

                    if ($key === 'callback') {
                        if (! $rule($this, $this->values)) {
                            $ok = false;
                            break;
                        }
                    } elseif ($rule == 'required') {
                        if (! $value) {
                            $this->errors[$name][] = _('Veuillez renseignez ce champ.');
                            $ok = false;
                            break;
                        }
                    } elseif ($rule == 'email') {
                        if (! filter_var($value, FILTER_VALIDATE_EMAIL)) {
                            $this->errors[$name][] = _("Cette adresse email n'est pas valide.");
                            $ok = false;
                            break;
                        }
                    } elseif ($rule == 'urlSlug') {
                        if (! preg_match('/^\w[\w-_\/]*$/', $value)) {
                            $this->errors[$name][] = _("Ce n'est pas une url valide");
                            $ok = false;
                            break;
                        }
                    } elseif ($rule == 'int') {
                        if (! preg_match('/^\d*$/', $value)) {
                            $this->errors[$name][] = _("Ce n'est pas un nombre entier");
                            $ok = false;
                            break;
                        }
                    } elseif ($rule == 'float') {
                        if (! preg_match('/^\d*([\.,]\d*)?$/', $value)) {
                            $this->errors[$name][] = _("Ce n'est pas un nombre");
                            $ok = false;
                            break;
                        }
                    }
                }
            }
        }

        return $ok;
    }

    public function getValue($name)
    {
        if (array_key_exists($name, $this->values)) {
            return $this->values[$name];
        }

        return null;
    }

    public function getValues()
    {
        return $this->values;
    }

    public function addError($name, $message)
    {
        if (array_key_exists($name, $this->errors)) {
            $this->errors[$name][] = $message;
        } else {
            $this->errors[$name] = array($message);
        }
    }

    public function getErrors($name)
    {
        if (array_key_exists($name, $this->errors)) {
            return $this->errors[$name];
        }

        return null;
    }

    public function hasError()
    {
        return count($this->errors);
    }

    public function addRules($rules)
    {
        $this->rules = array_merge($this->rules, $rules);
    }
}
