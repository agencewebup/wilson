<?php

namespace wilson;

class Image
{
    public $filename;
    public $gdim; // GD image
    public $width;
    public $height;

    public function __construct($filepath)
    {
        $this->filepath = $filepath;

        list($width, $height, $type) = getimagesize($filepath);
        $this->width = $width;
        $this->height = $height;

        switch ($type) {
            case IMAGETYPE_GIF:
                $this->gdim = imagecreatefromgif($filepath);
                break;
            case IMAGETYPE_JPEG:
                $this->gdim = imagecreatefromjpeg($filepath);
                break;
            case IMAGETYPE_PNG:
                $this->gdim = imagecreatefrompng($filepath);
                break;
            default:
                throw new \Exception('Image unknom type', 1);
        }
    }

    public function saveAspectFit($filepath, $width, $height ,$color)
    {

        $image = imagecreatetruecolor($width, $height);

        $col = imagecolorallocate($image, $color[0], $color[1], $color[2]);
        imagefilledrectangle($image, 0, 0, $width, $height, $col);


        // resize image
        $ratioWidth = $this->width / $width;
        $ratioHeight = $this->height / $height;

        if ($ratioWidth < $ratioHeight) {
            $ratio = $ratioHeight;
        } else {
            $ratio = $ratioWidth;
        }

        $dstWidth = (int) ($this->width / $ratio);
        $dstHeight = (int) ($this->height / $ratio);
        $dstX = (int) (($width - $dstWidth) / 2);
        $dstY = (int) (($height - $dstHeight) / 2);

        imagecopyresampled($image, $this->gdim, $dstX, $dstY, 0, 0, $dstWidth, $dstHeight, $this->width, $this->height);

        imagejpeg($image, $filepath, 80);
    }

    public function saveResize($filepath, $width, $height)
    {
        // calcul resize
        $sourceRatio = $this->width / $this->height;
        $ratio = $width / $height;

        if ($sourceRatio > $ratio) {
            $tmpHeight = $height;
            $tmpWidth = (int) ($height * $sourceRatio);
        } else {
            $tmpWidth = $width;
            $tmpHeight = (int) ($width / $sourceRatio);
        }

        // resizing
        $tmpGdim = imagecreatetruecolor($tmpWidth, $tmpHeight);
        imagecopyresampled(
            $tmpGdim,
            $this->gdim,
            0,
            0,
            0,
            0,
            $tmpWidth,
            $tmpHeight,
            $this->width,
            $this->height
        );

        // croping
        $x = ($tmpWidth - $width) / 2;
        $y = ($tmpHeight - $height) / 2;

        $newGdim = imagecreatetruecolor($width, $height);

        imagecopy(
            $newGdim,
            $tmpGdim,
            0,
            0,
            $x,
            $y,
            $width,
            $height
        );

        imagejpeg($newGdim, $filepath, 80);
    }
}
