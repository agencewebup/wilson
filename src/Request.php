<?php

namespace wilson;

class Request
{
    public function __construct()
    {

    }

    public function urlPath()
    {
        $path = null;
        $queryPos = strpos($_SERVER['REQUEST_URI'], '?');

        if ($queryPos) {
            list($path) = explode('?', $_SERVER['REQUEST_URI'], 2);
        } else {
            $path = $_SERVER['REQUEST_URI'];
        }

        return $path;
    }

    public function baseUrl()
    {
        $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
        $port = $_SERVER['SERVER_PORT'] == 80 ? '' : ':'.$_SERVER['SERVER_PORT'];

        return $protocol.$_SERVER['HTTP_HOST'].$port;
    }

    public function header($name)
    {
        $name = 'HTTP_' . strtoupper(str_replace('-', '_', $name));
        if (!empty($_SERVER[$name])) {
            return $_SERVER[$name];
        }

        return false;
    }
}
