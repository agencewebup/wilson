<?php

namespace wilson;

/**
 * Le Router centralise les urls
 *
 * @package wilson
 */
class Router
{
    /**
     * Préfix utilisé quand il n'y aucune zone de prefix définit
     */
    const DEFAULT_PREFIX = 'default';

    /**
     * Langue par défaut du Router
     * @var string
     */
    private $defaultLang;

    /**
     * Langue actuelle
     * @var string
     */
    private $lang;

    /**
     * Langues disponible
     * @var array
     */
    private $languages;

    /**
     * Dernier prefix utilisé pour l'ajout de route
     * @var string
     */
    private $addPrefix;

    /**
     * Préfix courant
     * @var string
     */
    private $prefix;

    /**
     * Liste de routes
     * @var array
     */
    private $routes;

    /**
     * Route courante
     * @var array
     */
    private $currentRoute;

    /**
     * Base url utiliser pour générer les url fullbase
     * @var string
     */
    private $baseUrl;

    /**
     * Contient le nom de controlleur qui gere les exceptions pour chaque prefix
     * @var array
     */
    private $exceptionHandlers;

    /**
     * Constructeur
     */
    public function __construct()
    {
        $this->addPrefix = self::DEFAULT_PREFIX;
        $this->prefix = self::DEFAULT_PREFIX;
        $this->routes = array(self::DEFAULT_PREFIX => array());
    }

    /**
     * Récupere une route depuis une URL (shortcut, args, controller, action, lang...)
     * @param  string $url
     * @return array  route
     */
    public function route($url)
    {
        $res = array(
            'url' => $url,
            'prefix' => self::DEFAULT_PREFIX,
            'lang' => $this->defaultLang
        );

        foreach ($this->routes as $prefix => $urls) {
            if ($prefix != self::DEFAULT_PREFIX && preg_match('@^/'.$prefix.'@', $url, $matches)) {
                $res['prefix'] = $prefix;
            }

            foreach ($urls as $shortcut => $route) {
                $pattern = $route['url'];

                if ($route['type'] == 'i18n') {
                    foreach ($route['url'] as $lang => $pattern) {
                        $matches = array();
                        if (preg_match('@^'.$pattern.'$@', $url, $matches)) {
                            return $this->makeRoute($url, $route, $matches, $shortcut, $prefix, $lang);
                        }
                    }
                } else {
                    if (preg_match('@^'.$pattern.'$@', $url, $matches)) {
                        return $this->makeRoute($url, $route, $matches, $shortcut, $prefix, $this->defaultLang);
                    }
                }
            }
        }

        return $res;
    }

    /**
     * Récupere une url depuis une route
     * @param  string|array $route    Le premier element du tableau est le shortcut.
     *                                La clé 'lang' pour la langue et 'prefix' pour le prefix.
     * @param  boolean $fullbase      Si true retourne l'url avec le nom de domaine
     * @return string  url
     */
    public function url($route, $fullbase = false)
    {
        $url = $route;

        if (is_array($route)) {
            $urlPattern = '';
            $lang = $this->lang();
            $prefix = $this->prefix;

            $shortcut = $route[0];
            unset($route[0]);

            if (isset($route['lang'])) {
                $lang = $route['lang'];
                unset($route['lang']);
            }

            if (isset($route['prefix'])) {
                $prefix = $route['prefix'];
                unset($route['prefix']);
            }

            if ($this->routes[$prefix][$shortcut]['type'] == 'i18n') {
                $urlPattern = $this->routes[$prefix][$shortcut]['url'][$lang];
            } else {
                $urlPattern = $this->routes[$prefix][$shortcut]['url'];
            }

            $url = preg_replace_callback(
                '/\(\?P<(\w*)>.*\)/U',
                function ($match) use ($route) {
                    if (count($match) && array_key_exists($match[1], $route)) {
                        return $route[$match[1]];
                    }
                    return;
                },
                $urlPattern
            );
        }

        if ($fullbase) {
            $url = $this->baseUrl . $url;
        }

        return $url;
    }

    /**
     * Tout les urls ajouter entre startPrefix() et endPrefix() sont préfixé de $prefix.
     * Les prefix agissent un peu comme de namespace
     * @param string $prefix
     * @return $this
     */
    public function startPrefix($prefix)
    {
        $this->addPrefix = $prefix;

        return $this;
    }

    /**
     * Fin de la zone de préfix
     * @return $this
     */
    public function endPrefix()
    {
        $this->addPrefix = self::DEFAULT_PREFIX;

        return $this;
    }

    /**
     * Ajout une route
     * @param string $shortname  Attention doit être unique par par prefix sinon le dernier prend le dessus
     * @param string $url        regex de l'url
     * @param string $controller \namespace\Constroller.action
     */
    public function add($shortname, $url, $controller)
    {
        if ($this->addPrefix != self::DEFAULT_PREFIX) {
            $url = '/' . $this->addPrefix . $url;
        }

        $this->routes[$this->addPrefix][$shortname] = array(
            'type' => 'url',
            'controller' => $controller,
            'url' => $url,
        );

        return $this;
    }

    /**
     * Ajoute une url qui peut etre traduite
     * @param string $shortname  Attention doit être unique par par prefix sinon le dernier prend le dessus
     * @param string|array $urls  regex de l'url ou tableau contenant les url de chaque langue
     * @param string $controller \namespace\Constroller.action
     */
    public function addI18n($shortname, $urls, $controller)
    {
        if (is_array($urls)) {
            foreach ($urls as $lang => $url) {
                if ($this->addPrefix != self::DEFAULT_PREFIX) {
                    $url = '/' . $this->addPrefix . $url;
                }

                $urls[$lang] = '/'. $lang . $url;
            }
        } else {
            $url = $urls;

            if ($this->addPrefix != self::DEFAULT_PREFIX) {
                $url = '/' . $this->addPrefix . $url;
            }

            $urls = array();
            foreach ($this->languages as $lang => $locale) {
                $urls[$lang] = '/'. $lang . $url;
            }
        }

        $this->routes[$this->addPrefix][$shortname] = array(
            'type' => 'i18n',
            'controller' => $controller,
            'url' => $urls
        );

        return $this;
    }

    /**
     * Charger un fichier de routes
     * @param  string $file path
     * @return $this
     */
    public function import($file)
    {
        include($file);
        return $this;
    }


    /**
     * Récupere le nom du controller qui gere les execption pour le prefix courant
     * @return string Nom du controller
     */
    public function exceptionHandler()
    {
        $controller = null;
        if (isset($this->exceptionHandlers[$this->prefix])) {
            $controller = $this->exceptionHandlers[$this->prefix];
        }

        return $controller;
    }

    /**
     * Set le controlleur qui gere les exceptions pour le prefix courant
     * @param string $controller Nom et namespace du controlleur
     * @return $this
     */
    public function setExceptionHandler($controller)
    {
        $this->exceptionHandlers[$this->addPrefix] = $controller;

        return $this;
    }

    /**
     * Set la route courante
     * @param array $route route
     */
    public function setCurrentRoute($route)
    {
        $this->currentRoute = $route;
    }

    /**
     * Retourne la route courante
     * @return array
     */
    public function currentRoute()
    {
        return $this->currentRoute;
    }

    /**
     * Récupere la lang par defaut
     * @return string
     */
    public function defaultLang()
    {
        return $this->defaultLang;
    }

    /**
     * Set la langue par defaut
     * @param string $lang c'est la clé dans le tableau des langues cf: setLanguages()
     */
    public function setDefaultLang($lang)
    {
        $this->defaultLang = $lang;
    }

    /**
     * Retourne la langue courante
     * @return string
     */
    public function lang()
    {
        if ($this->lang === null) {
            $this->lang = $this->defaultLang;
        }

        return $this->lang;
    }

    /**
     * Set la lang courante
     * @param string $lang
     */
    public function setLang($lang)
    {
        $this->lang = $lang;
    }

    /**
     * Set les langs disponible
     * @param array $langs exemple : array('fr' => 'fr_FR.UTF-8')
     */
    public function setLanguages($langs)
    {
        $this->languages = $langs;
    }

    /**
     * Retourne les langues disponibles
     * @return array Clé: nom de la langue. Value: info complémentaire
     */
    public function languages()
    {
        return $this->languages;
    }

    /**
     * Set le prefix courant
     * @param string $prefix
     */
    public function setPrefix($prefix)
    {
        $this->prefix = $prefix;
    }

    /**
     * Set l'url de base qui permet de génerer les url absolue
     * @param string $url base url
     */
    public function setBaseUrl($url)
    {
        $this->baseUrl = $url;
    }

    /**
     * Retourne une belle route
     * @todo  uniformiser avec la route qu'on passe dans la fonction url()
     * @param  [type] $url
     * @param  [type] $route
     * @param  [type] $matches
     * @param  [type] $shortcut
     * @param  [type] $prefix
     * @param  [type] $lang
     * @return [type]
     */
    private function makeRoute($url, $route, $matches, $shortcut, $prefix, $lang)
    {
        $args = array();
        foreach ($matches as $key => $value) {
            if (! is_numeric($key)) {
                $args[$key] = $value;
            }
        }
        $atom = explode('.', $route['controller']);

        $ctrl = explode("\\", $atom[0]);
        $ctrl = end($ctrl);

        $route = array($shortcut);
        $route = array_merge($route, $args);
        $route['prefix'] = $prefix;
        $route['lang'] = $lang;

        $callback = array(
            'url' => $url,
            'route' => $route,
            'shortcut' => $shortcut,
            'controller' => $atom[0],
            'action' => $atom[1],
            'args' => $args,
            'prefix' => $prefix,
            'lang' => $lang,
            'controllerName' => $ctrl
        );

        return $callback;
    }
}
