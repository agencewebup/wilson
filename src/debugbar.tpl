<div class="debugbar">
    <div class="debugbar-head">
        <a href="" data-id="1" class="debugbar-tab">Errors <span class="debugbar-badge debugbar-red"><?php echo count(DebugBar::$errors) ?></span></a>
        <a href="" data-id="2" class="debugbar-tab">Database <span class="debugbar-badge debugbar-green"><?php echo count(DebugBar::$database) ?></span></a>
        <a href="" data-id="3" class="debugbar-tab">SEO</a>
        <a href="" class="debugbar-close">Close<span class="debugbar-close-button">x</span></a>
    </div>
    <div class="debugbar-body">
        <div id="js-debugbar-panel-1" class="debugbar-panel">
            <?php foreach (DebugBar::$errors as $error): ?>
            <div class="debugbar-line">
                <?php echo nl2br($error['errstr']) . ' File : ' . $error['errfile'] .' Line : '. $error['errline'] ; ?>
            </div>
            <?php endforeach; ?>
        </div>
        <div id="js-debugbar-panel-2" class="debugbar-panel">
            <?php foreach (DebugBar::$database as $db): ?>
            <div class="debugbar-line">
                <?php echo $db; ?>
            </div>
            <?php endforeach; ?>
        </div>
        <div id="js-debugbar-panel-3" class="debugbar-panel">
            <div class="debugbar-line">
                <?php echo DebugBar::$seo['title'] . ' Count : ' . strlen(DebugBar::$seo['title']); ?>
            </div>
            <div class="debugbar-line">
                <?php echo DebugBar::$seo['description'] . ' Count : ' . strlen(DebugBar::$seo['description']); ?>
            </div>
        </div>
    </div>
</div>

<style type="text/css">

    .debugbar {
        position: fixed;
        bottom: 0;
        left: 0;
        right: 0;
        background: #fff;
        font-family: Futura, "Trebuchet MS", Arial, sans-serif;
        font-size: 15px !important;
        z-index: 1000;
    }

    .debugbar-badge {
        background-color: #afafaf;
        padding: 0 8px;
        display: inline-block;
        border-radius: 4px;
    }

    .debugbar-green {
        background-color: #5CB85C;
        color: #fff;
    }

    .debugbar-red {
        background-color: #D9534F;
        color: #fff;
    }


    .debugbar-head {
        background: #fcfcfc;
        border-top: 1px solid #0C0C0C;
        border-bottom: 1px solid #0C0C0C;
    }

    .debugbar-head a {
        color: #0c0c0c !important;
    }

    .debugbar-tab {
        display: inline-block;
        padding: 5px 20px;
        border-right: 1px solid grey;
    }

    .debugbar-close {
        float: right;
        padding: 5px 20px;
    }

    .debugbar-close-button {
        display: inline-block;
        height: 20px;
        width: 20px;
        text-align: center;
        line-height: 14px;
        vertical-align: middle;
        border-radius: 50%;
        border: 1px solid #0C0C0C;
        margin: 0 4px;
    }

    .debugbar-close {
    }

    .debugbar-body {
        height: 300px;
        overflow:scroll;
    }

    .debugbar-panel {
    }

    .debugbar-line {
        background: #fff;
        padding: 3px 10px;
    }

    .debugbar-line:nth-child(odd) {
        background: #f4fffd;
    }

</style>

<script type="text/javascript">

    var body = document.getElementsByClassName('debugbar-body')[0];

    if (readCookie('debugbar-tab')) {
        var panels = document.getElementsByClassName('debugbar-panel');
        for (var j = 0; j < panels.length; j++) {
            panels[j].style.display = 'none';
        }

        var panel = document.getElementById(readCookie('debugbar-tab'));
        panel.style.display = 'block';
    } else {
        body.style.display = 'none';
    }

    var close = document.getElementsByClassName('debugbar-close')[0];
    close.onclick = function(ev) {
        ev.preventDefault();
        body.style.display = 'none';
        eraseCookie('debugbar-tab');
    };

    var tabs = document.getElementsByClassName('debugbar-tab');
    for (var i = 0; i < tabs.length; i++) {
        tabs[i].onclick = function(ev) {
            ev.preventDefault();

            body.style.display = 'block';

            var panels = document.getElementsByClassName('debugbar-panel');
            for (var j = 0; j < panels.length; j++) {
                panels[j].style.display = 'none';
            }

            var panel = document.getElementById('js-debugbar-panel-'+this.getAttribute('data-id'));
            panel.style.display = 'block';

            createCookie('debugbar-tab', 'js-debugbar-panel-'+this.getAttribute('data-id'));
        };
    };

    function createCookie(name,value,days) {
        if (days) {
            var date = new Date();
            date.setTime(date.getTime()+(days*24*60*60*1000));
            var expires = "; expires="+date.toGMTString();
        }
        else var expires = "";
        document.cookie = name+"="+value+expires+"; path=/";
    }

    function readCookie(name) {
        var nameEQ = name + "=";
        var ca = document.cookie.split(';');
        for(var i=0;i < ca.length;i++) {
            var c = ca[i];
            while (c.charAt(0)==' ') c = c.substring(1,c.length);
            if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
        }
        return null;
    }

    function eraseCookie(name) {
        createCookie(name,"",-1);
    }

</script>
