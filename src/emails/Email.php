<?php

namespace wilson\emails;

/**
 * Cette classe est utilisé pour envoyer des emails
 * Pour plus d'information voir https://github.com/PHPMailer/PHPMailer
 * @package wilson
 */
class Email extends \PHPMailer
{
    /**
     * @override
     * @param SmtpConfig config Configuration SMTP
     */
    public function __construct($config)
    {
        parent::__construct(true);

        $this->CharSet = 'UTF-8';
        $this->isSMTP();

        $this->Host = $config->host;
        $this->Port = $config->port;

        $this->SMTPAuth = $config->useAuth;
        $this->SMTPSecure = $config->protocol;

        $this->Username = $config->username;
        $this->Password = $config->password;

        $this->From = $config->from;
        $this->FromName = $config->fromName;
    }
}
