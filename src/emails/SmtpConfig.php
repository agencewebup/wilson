<?php

namespace wilson\emails;

class SmtpConfig
{
    /**
     * Host du serveur smtp
     * @var string
     */
    public $host = '';

    /**
     * Port du serveur smtp
     * @var integer
     */
    public $port = 25;

    /**
     * Utilise l'authentification smtp
     * @var boolean
     */
    public $useAuth = false;

    /**
     * Prefixe de connexion sécurisé
     * Options: "", "ssl" or "tls"
     * @var String
     */
    public $protocol = '';

    /**
     * User du smtp
     * @var string
     */
    public $username = '';

    /**
     * Mot de passe du stmp
     * @var string
     */
    public $password = '';

    /**
     * Adresse email de l'expéditeur
     * @var string
     */
    public $from = '';

    /**
     * Nom de l'expéditeur
     * @var string
     */
    public $fromName = '';
}
