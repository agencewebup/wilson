<?php

namespace wilson;

class Exception extends \Exception {}

// 403 forbiden
class ForbiddenException extends \Exception {}

// 404 not found
class NotFoundException extends \Exception {}

class ModelException extends \Exception {}

class DSException extends \Exception {}

class DSConnectionException extends \Exception {}
