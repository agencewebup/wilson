<?php

function debug($var)
{
    echo '<pre>';
    if (is_array($var)) {
        print_r($var);
    } else {
        var_dump($var);
    }

    echo '</pre>';
}

function backtrace()
{
    $str = '';
    $bt = debug_backtrace();
    for ($i=1; $i < count($bt); $i++) {
        $function = isset($bt[$i]['class']) ? $bt[$i]['class'] . '::' : '';
        $function .= $bt[$i]['function'] . '()';

        $str .= '#'.$i . ' ' .$bt[$i]['file']. '(' .$bt[$i]['line']. '): ' .$function.  "\n";
    }

    return $str;
}

function h($string)
{
    return htmlspecialchars($string);
}

function e($string)
{
    echo htmlspecialchars($string);
}
