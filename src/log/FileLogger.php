<?php

namespace wilson\log;

class FileLogger
{
    protected $stream;


    public function __construct($filename)
    {
        $this->stream = fopen($filename, 'a');
    }

    public function __destruct()
    {
        fclose($this->stream);
    }

    public function write($type, $message)
    {
        $output = date('Y-m-d H:i:s') . ' ' . ucfirst($type) . ': ' . $message . "\n";
        fwrite($this->stream, $output);
    }
}
