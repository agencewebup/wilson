<?php

namespace wilson\log;

class Log
{
    protected static $loggers = array();

    const EMERGENCY = 'emergency';
    const ALERT     = 'alert';
    const CRITICAL  = 'critical';
    const ERROR     = 'error';
    const WARNING   = 'warning';
    const NOTICE    = 'notice';
    const INFO      = 'info';
    const DEBUG     = 'debug';

    public static function addLogger($logger)
    {
        self::$loggers[] = $logger;
    }

    public static function removeLogger($logger)
    {
        if(($key = array_search($logger, self::$loggers, true)) !== FALSE) {
            unset(self::$loggers[$key]);
        }
    }

    public static function getLoggers()
    {
        return self::$loggers;
    }

    /**
     * Logs avec un niveau arbitraire.
     *
     * @param mixed $level
     * @param string $message
     * @return void
     */
    public static function write($level, $message)
    {
        foreach (self::$loggers as $logger) {
            $logger->write($level, $message);
        }
    }

    /**
     * Le système est inutilisable.
     *
     * @param string $message
     * @return void
     */
    public static function emergency($message)
    {
        self::write(self::EMERGENCY, $message);
    }

    /**
     * Des mesures doivent être prises immédiatement.
     *
     * Exemple: Tout le site est hors service, la base de données est indisponible, etc. Cela devrait
     * déclencher des alertes par SMS et vous réveiller.
     *
     * @param string $message
     * @return void
     */
    public static function alert($message)
    {
        self::write(self::ALERT, $message);
    }

    /**
     * Conditions critiques.
     *
     * Exemple: Composant d'application indisponible, exception inattendue.
     *
     * @param string $message
     * @return void
     */
    public static function critical($message)
    {
        self::write(self::CRITICAL, $message);
    }

    /**
     * Erreurs d'exécution qui ne nécessitent pas une action immédiate mais doit normalement
     * être journalisée et contrôlée.
     *
     * @param string $message
     * @return void
     */
    public static function error($message)
    {
        self::write(self::ERROR, $message);
    }

    /**
     * Événements exceptionnels qui ne sont pas des erreurs.
     *
     * Exemple: Utilisation des API obsolètes, mauvaise utilisation d'une API, indésirables élements
     * qui ne sont pas nécessairement mauvais.
     *
     * @param string $message
     * @return void
     */
    public static function warning($message)
    {
        self::write(self::WARNING, $message);
    }

    /**
     * Événements normaux mais significatifs.
     *
     * @param string $message
     * @return void
     */
    public static function notice($message)
    {
        self::write(self::NOTICE, $message);
    }

    /**
     * Événements intéressants.
     *
     * Exemple: Connexion utilisateur, journaux SQL.
     *
     * @param string $message
     * @return void
     */
    public static function info($message)
    {
        self::write(self::INFO, $message);
    }

    /**
     * Informations détaillées de débogage.
     *
     * @param string $message
     * @return void
     */
    public static function debug($message)
    {
        self::write(self::DEBUG, $message);
    }
}
