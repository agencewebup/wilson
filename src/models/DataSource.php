<?php

namespace wilson\models;

abstract class DataSource
{
    protected $db;

    /**
     * Configuration du datasource
     * @var array
     */
    public $config = array();

    /**
     * Constructeur
     * @param array $config configuration
     */
    public function __construct($config)
    {
        $this->config = $config;
    }

    /**
     * Connexion à la base
     */
    abstract public function connect();

    /**
     * Déconnexion de la base
     */
    abstract public function disconnect();

    /**
     * Creer un enregistrement dans la base. Le "C" de CRUD
     *
     * @param  string $table Nom de la table
     * @param  array  $data  Tableau associatif en clé le fields et en valeur la valeur
     */
    public function create($table, $data)
    {
        $fields = array();
        foreach ($data as $key => $value) {
            $fields[] = '`'.$key.'`';
        }

        $sql  = 'INSERT INTO `' . $table.'`';
        $sql .= ' (' . implode(', ', $fields) . ')';
        $sql .= ' VALUES (' . implode(', ', array_fill(0, count($data), '?')) . ')';

        $this->execute($sql, $data);

        return $this->db->lastInsertId();
    }

    /**
     * Lit les informations de la base. Le "R" de CRUD
     *
     * @param  array $query Tableau associatif des paramtres de la requete
     * @return array        Résultat de la recherhce
     */
    public function read($query)
    {
        $select = '*';

        if ($query['select'] != null) {
            $select = $query['select'];
        }

        $sql = 'SELECT ' . $select . ' FROM `' . $query['from'] . '`';
        $bindings = array();

        if ($query['join'] != null) {
            $sql.= ' ' . implode(' ', $query['join']);
        }

        if ($query['where'] != null) {
            $sql.= ' WHERE ' . $query['where'];
        }

        if ($query['groupby'] != null) {
            $sql.= ' GROUP BY ' . $query['groupby'];
        }

        if ($query['having'] != null) {
            $sql.= ' HAVING ' . $query['having'];
        }

        if ($query['order'] != null) {
            $sql.= ' ORDER BY ' . $query['order'];
        }

        if ($query['limit'] != null) {
            $sql.= ' LIMIT ' . $query['limit'];
        }

        $statement = $this->execute($sql, $query['bindings']);
        return $this->makeModel($statement, $query['models']);
    }

    /**
     * Modifie un enregistrement dans la base Le "U" de CRUD
     *
     * @param  string $table      Nom de la table
     * @param  array  $data       Tableau associatif en clé le fields et en valeur la valeur
     * @param  string $conditions Clause WHERE en SQL
     * @param  array  bindings    Variables à associer
     */
    public function update($table, $data, $conditions = null, $bindings = array())
    {
        if (count($data)) {
            $fields = array();
            foreach ($data as $key => $value) {
                $fields[$key] = '`'.$key.'`= ?';
            }

            $sql = 'UPDATE `' . $table.'`';
            $sql.= ' SET '. implode(', ', $fields);

            if ($conditions != null) {
                $sql.= ' WHERE ' . $conditions;
                foreach ($bindings as $key => $value) {
                    $data[] = $value;
                }
            }

            $this->execute($sql, $data);
        }
    }

    /**
     * Savegarde un model dans la base
     * @param Model $model Classe modele
     */
    public function save($model)
    {
        $properties = $model->getProperties();
        unset($properties['id']);

        if ($model->id()) {
            $this->update($model::$table, $properties, 'id = ?', array($model->id()));
        } else {
            $model->id($this->create($model::$table, $properties));
        }
    }

    /**
     * Savegarde un model dans la base
     * @param Model $model Classe modele
     */
    public function delete($model)
    {
        $this->deleteWithConditions($model::$table, 'id = ?', array($model->id()));
    }

    /**
     * Supprimmer un enregistrement Le "D" de CRUD
     *
     * @param  string $table      Nom de la table
     * @param  string $conditions Clause WHERE en SQL
     * @param  array  bindings    Variables à associer
     */
    public function deleteWithConditions($table, $conditions = null, $bindings = array())
    {
        $sql = 'DELETE FROM `' . $table.'`';

        if ($conditions != null) {
            $sql.= ' WHERE ' . $conditions;
        }

        $this->execute($sql, $bindings);
    }

    /**
     * Retourne le dernier id inséré
     *
     * @return int id
     */
    public function lastInsertId()
    {
        return $this->db->lastInsertId();
    }

    public function beginTransaction()
    {
        if (!$this->db->inTransaction()) {
            return false;
        }

        return $this->db->beginTransaction();
    }

    public function commitTransaction()
    {
        if (!$this->db->inTransaction()) {
            return false;
        }

        return $this->db->commit();
    }

    public function rollBackTransaction()
    {
        if (!$this->db->inTransaction()) {
            return false;
        }

        return $this->db->rollBack();
    }

    /**
     * retourne le shema de la table
     * @param  string $table Nom de la table
     * @return array         description de la table
     */
    abstract public function describe($table);

    /**
     * Retourne un objet Statement pour faire une recherche dans la base
     *
     * @param string $model Nom du model
     * @return Statement
     */
    public function all($model)
    {
        return new Statement($this, $model::$table, $model);
    }

    /**
     * Execute une requete
     *
     * @param  string $sql      Requete SQL
     * @param  array  $bindings Valeur à binder de la requete
     * @return PDOStatement
     */
    public function execute($sql, $bindings = array())
    {
        $args = array();
        $flatBindings = array();

        foreach ($bindings as $key => $value) {
            if (is_array($value)) {
                $args[] = '('.implode(',', array_fill(0, count($value), '?')).')';
                foreach ($value as $v) {
                    $flatBindings[] = $v;
                }
            } else {
                $args[] = '?';
                $flatBindings[] = $value;
            }
        }

        $sql = vsprintf(str_replace('?', '%s', $sql), $args);

        if ($this->config['debug'] && class_exists('DebugBar', false)) {
            \DebugBar::request($this->debugQuery($sql, $flatBindings));
        }

        try {
            $statement = $this->db->prepare($sql);

            foreach ($flatBindings as $key => $value) {
                if (is_int($key)) {
                    ++$key;
                } else {
                    $key = ':' . $key;
                }
                $statement->bindValue($key, $value);
            }

            $statement->execute();

        } catch (\PDOException $e) {
            throw new \wilson\DSException($e->getMessage(), 1, $e);
        }

        return $statement;
    }

    public function makeModel($statement, $models)
    {
        $res = array();

        $result = $statement->fetchAll(\PDO::FETCH_NUM);

        if (count($result)) {
            $map = $this->getMap($statement, $models);
            //debug($map);

            foreach ($result as $row) {
                $this->fetchRow($row, $models, $map, $res);
            }
        }

        foreach ($res as &$model) {
            foreach ($model->associations as &$a) {
                if (is_array($a)) {
                    $a = array_values($a);
                }
            }
        }

        return array_values($res);
    }

    public function fetchRow($row, $models, $map, &$res)
    {
        $class = $models[0];
        $object = new $class();

        $rels = array();

        foreach ($row as $i => $col) {

            if ($map[$i]['table'] == $class::$table) {
                $object->$map[$i]['name']($col);

                if ($map[$i]['name'] == 'id') {
                    if (array_key_exists($col, $res)) {
                        $object = $res[$col];
                    }
                }

            // relationships
            } else {

                $model = null;

                foreach ($models as $m) {
                    if ($map[$i]['table'] == $m::$table) {
                        $model = $m;
                        break;
                    }
                }

                if ($model) {

                    if (array_key_exists($model, $rels)) {
                        $plop = $rels[$model];
                        $plop->$map[$i]['name']($col);

                    } else {
                        $plop = new $model();
                        $plop->$map[$i]['name']($col);

                        $rels[$model] = $plop;
                    }
                }
            }
        }

        // associations
        foreach ($rels as $key => $value) {

            foreach ($models as $i => $model) {
                foreach ($model::$relationships as $f => $r) {

                    if ($key == $r['model']) {

                        $obj = &$object;

                        if ($i > 0) {
                            $obj = &$rels[$model];
                        }

                        if ($r['type'] == 'belongsTo' || $r['type'] == 'hasOne') {
                            $obj->associations[$f] = $value;

                        } elseif ($r['type'] == 'hasMany' || $r['type'] == 'hasManyThrough') {
                            if (! array_key_exists($f, $obj->associations)) {
                                $obj->associations[$f] = array();
                            }

                            if ($value->id()) {
                                $obj->associations[$f][$value->id()] = $value;
                            }
                        }

                        break 2;
                    }

                }
            }

        }

        $res[$object->id()] = $object;
    }

    public function debugQuery($query, $params) {
        $keys = array();

        foreach ($params as $key => $value) {
            $params[$key] = "'".$value."'";
            if (is_string($key)) {
                $keys[] = '/:'.$key.'/';
            } else {
                $keys[] = '/[?]/';
            }
        }

        $query = preg_replace($keys, $params, $query, 1, $count);

        return $query;
    }

    abstract public function getMap($statement, $models);
}
