<?php

namespace wilson\models;

class Database
{
    /**
     * singleton
     * @var Database
     */
    private static $sharedInstance = null;

    /**
     * Stock les bases de données
     * @var array
     */
    private $dbs = array();

    /**
     * Constructeur privé
     */
    private function __construct()
    {
    }

    /**
     * Destructeur
     */
    public function __destruct()
    {
        foreach ($this->dbs as $db) {
            $db->disconnect();
        }
    }

    /**
     * Récupère le base désirée selon le nom de sa config
     *
     * @param  string $name nom de la config
     * @return Datasource
     */
    public static function getdb($name = 'default')
    {
        if (self::$sharedInstance === null) {
            self::$sharedInstance = new Database();
        }

        if (! array_key_exists($name, self::$sharedInstance->dbs)) {
            throw new \Exception("Veuillez instancier la db manuellement en utilisant Database::setdb", 1);
        }

        return self::$sharedInstance->dbs[$name];
    }

    /**
     * Ajoute une db au dépot
     * @param  string     $name
     * @param  Datasource $db
     */
    public static function setdb($name, $db)
    {
        self::$sharedInstance->dbs[$name] = $db;
    }
}
