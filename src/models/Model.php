<?php

namespace wilson\models;

/**
 * @todo à optimiser pour que les jointures soient pre-chargés
 */

class Model
{
    /**
     * Nom de la db à utiliser
     * @var string
     * @deprecated Remove on next version.
     */
    public static $config = 'default';

    /**
     * Nom de la table sql
     * @var string
     */
    public static $table;

    /**
     * Définition des jointures
     * @var array
     */
    public static $relationships = array();

    /**
     * Modèles associés
     * @var array
     */
    public $associations = array();

    /**
     * Constructeur
     */
    public function __construct()
    {
    }

    // Appel des getters setters automatiquement
    public function __call($function, $args)
    {
        if (empty($args)) {
            // getter
            $func = 'get' . ucfirst($function);
            if (method_exists($this, $func)) {
                // si c'est une méthode
                return $this->func();

            } elseif (property_exists($this, $function)) {
                // si c'est une propriété
                return $this->$function;

            } elseif (array_key_exists($function, $this->associations)) {
                // si c'est une jointure
                return $this->associations[$function];
            }

        } else {
            // setter
            $func = 'set' . ucfirst($function);
            if (method_exists($this, $func)) {
                // si c'est une méthods
                return $this->$func($args[0]);

            } elseif (property_exists($this, $function)) {
                // si c'est une propriété
                return $this->$function = $args[0];

            } elseif (array_key_exists($function, static::$relationships)) {
                // si c'est une jointure
                return $this->associations[$function] = $args[0];
            }
        }

        // error
        $backtrace = debug_backtrace();
        $error = sprintf(
            'Fatal error: Call to undefined method %s::%s() in %s on line %s',
            get_called_class(),
            $function,
            $backtrace[0]['file'],
            $backtrace[0]['line']
        );
        trigger_error($error, E_USER_ERROR);
        die();
    }

    /**
     * Prend un tableau associatif et set les propriétés avec
     *
     * @param array $properties
     */
    public function setProperties($properties)
    {
        $vars = get_object_vars($this);

        foreach ($vars as $key => $value) {
            if (array_key_exists($key, $properties)) {
                $this->$key($properties[$key]);
            }
        }
    }

    public function getProperties()
    {
        $p = get_object_vars($this);
        unset($p['associations']);
        return $p;
    }

    /**
     * savegarde le model dans la base
     * @deprecated Remove on next version. Utiliser la fonction save($model) de Datasource sinon c'est pas unit testable
     */
    public function save()
    {
        trigger_error('Model::save() is deprecated.');
        self::getDatabase()->save($this);
    }

    /**
     * Supprime l'enregistrement de la base
     * @deprecated Remove on next version. Utiliser la fonction delete($model) de Datasource sinon c'est pas unit testable
     */
    public function delete()
    {
        trigger_error('Model::delete() is deprecated.');
        self::getDatabase()->delete($this);
    }

    /**
     * Retourne un objet Statement pour faire une recherche dans la base
     * @deprecated Remove on next version. Utiliser la fonction all($model) de Datasource sinon c'est pas unit testable
     *
     * @return Statement
     */
    public static function all()
    {
        trigger_error('Model::all() is deprecated.');
        return self::getDatabase()->all(get_called_class());
    }

    /**
     * Retourne un model s'il y trouve l'id, sinon throw error
     * @deprecated Remove on next version. Utiliser la fonction all($model) de Datasource sinon c'est pas unit testable
     *
     * @param  mixed   $id soit c'est un id ou c'est un array de conditions
     * @return Model     [description]
     */
    public static function get($id)
    {
        trigger_error('Model::get() is deprecated.');
        $statement = self::getDatabase()->all(get_called_class());

        if (is_array($id)) {
            $where = array();
            $params = array();

            foreach ($id as $key => $value) {
                $where[] = $key . ' = :' . $key;
                $params[$key] = $value;
            }

            $statement->where(implode(' AND ', $where), $params)->limit(1);
        } else {
            $statement->where('id = :id', array('id' => $id))->limit(1);
        }

        if (count($statement) == 0) {
            throw new \wilson\ModelException('Object not found', 1);
        }

        return $statement[0];
    }

    /**
     * @deprecated Remove on next version. Utiliser la fonction all($model) de Datasource sinon c'est pas unit testable
     */
    public static function count()
    {
        trigger_error('Model::count() is deprecated.');
        $statement = self::getDatabase()->execute('SELECT COUNT(id) FROM ' . static::$table);
        $result = $statement->fetchAll();

        return $result[0][0];
    }

    /**
     * @deprecated Remove on next version. Utiliser la fonction all($model) de Datasource sinon c'est pas unit testable
     */
    public static function listOptions($field, $empty = null)
    {
        trigger_error('Model::listOptions() is deprecated.');
        $statement = static::all();

        $list = array();

        if ($empty !== null) {
            $list[''] = $empty;
        }

        foreach ($statement as $object) {
            $list[$object->id()] = $object->$field();
        }

        return $list;
    }

    /**
     * Retourne le model qui a la plus petite valeur pour un certain champs
     * @deprecated Remove on next version.
     * @param  string $column nom du champs
     * @return Model          [description]
     */
    public static function getLowerValue($column)
    {
        trigger_error('Model::getLowerValue() is deprecated.');
        $statement = self::getDatabase()->all(get_called_class());
        $statement->order($column)->limit(1);

        if (count($statement) == 0) {
            throw new \wilson\ModelException('Object not found', 1);
        }

        return $statement[0];
    }

    /**
     * Retourne le model qui a la plus grande valeur pour un certain champs
     * @deprecated Remove on next version.
     * @param  string $column nom du champs
     * @return Model          [description]
     */
    public static function getHighterValue($column)
    {
        trigger_error('Model::getHighterValue() is deprecated.');
        $statement = self::getDatabase()->all(get_called_class());
        $statement->order($column . ' DESC')->limit(1);

        if (count($statement) == 0) {
            throw new \wilson\ModelException('Object not found', 1);
        }

        return $statement[0];
    }

    /**
     * @deprecated Remove on next version.
     */
    public static function describe()
    {
        trigger_error('Model::describe() is deprecated.');
        if (static::$table != null) {
            return self::getDatabase()->describe(static::$table);
        } else {
            return null;
        }
    }

    /**
     * @deprecated Remove on next version.
     */
    public static function getConfig()
    {
        trigger_error('Model::getConfig() is deprecated.');
        return static::$config;
    }

    /**
     * @deprecated Remove on next version. On recupere la db à partir de la classe Database
     */
    public static function getDatabase()
    {
        trigger_error('Model::getDatabase() is deprecated.');
        return Database::getdb(self::getConfig());
    }
}
