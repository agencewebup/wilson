<?php

namespace wilson\models;

class Mysql extends DataSource
{

    ///   ---   Connexion   ---   ///

    public function connect()
    {
        try {
            $dsn = 'mysql:host=' . $this->config['host'] . ';port=' . $this->config['port'] . ';dbname=' . $this->config['database'];
            $username = $this->config['username'];
            $password = $this->config['password'];
            $setUtf8 = array(\PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8");

            $this->db = new \PDO($dsn, $username, $password, $setUtf8);
            $this->db->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        } catch (\PDOException $e) {
            throw new \wilson\DSConnectionException('Connection error '.$e->getMessage());
        }
    }

    public function disconnect()
    {
        $this->db = null;
    }

    ///   ---   Methods   ---   ///

    public function describe($table)
    {
        $sql = sprintf('DESCRIBE `%s`', $table);
        $statement = $this->execute($sql);

        return $statement->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function getMap($statement, $models)
    {
        $map = array();

        $columnCount = $statement->columnCount();

        for ($i = 0; $i < $columnCount; $i++) {
            $map[] = $statement->getColumnMeta($i);
        }

        return $map;
    }
}
