<?php

namespace wilson\models;

class Sqlite extends DataSource
{

    ///   ---   Connexion   ---   ///

    public function connect()
    {
        try {
            $dns = 'sqlite:' . $this->config['database'];
            $this->db = new \PDO($dns);
            $this->db->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        } catch (\PDOException $e) {
            throw new \wilson\DSConnectionException('Connection error '.$e->getMessage());
        }
    }

    public function disconnect()
    {
        $this->db = null;
    }

    ///   ---   Methods   ---   ///

    public function describe($table)
    {
        $sql = sprintf('pragma table_info(%s);', $table);
        $statement = $this->execute($sql);

        return $statement->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function getMap($statement, $models)
    {
        $map = array();

        $columnCount = $statement->columnCount();

        foreach ($models as $model) {
            $desc = $this->describe($model::$table);

            foreach ($desc as $d) {
                $map[] = array(
                    'table' => $model::$table,
                    'name' => $d['name'],
                );
            }
        }

        return $map;
    }
}
