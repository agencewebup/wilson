<?php

namespace wilson\models;

class Statement implements \Iterator, \arrayaccess, \Countable
{
    /**
     * Résultat de la recherche. Utilisé par l'Itérateur.
     * @var array
     */
    private $result;

    /**
     * Contient les informations de la requete
     * @var array
     */
    private $query;

    /**
     * Constructeur
     * @param Datasource $database Database
     * @param string     $table    Nom de la table à requeter
     * @param string     $class    Nom de la class model à retourner
     */
    public function __construct($database, $table, $class)
    {
        $this->database = $database;
        $this->class = $class;
        $this->table = $table;

        $this->query = array(
            'select' => null,
            'from' => $table,
            'join' => array(),
            'where' => null,
            'groupby' => null,
            'having' => null,
            'order' => null,
            'limit' => null,
            'bindings' => array(),
            'models' => array($class),
        );
    }

    ///   ---   Query   ---   ///

    /**
     * Execute la requete dans la base
     */
    public function execute()
    {
        $this->result = $this->database->read($this->query);

        return $this;
    }

    public function first()
    {
        $object = null;

        $this->execute();

        if (count($this->result) != 0) {
            $object = $this->result[0];
        }

        return $object;
    }

    public function select($select)
    {
        $this->query['select'] = $select;

        return $this;
    }

    public function join($model)
    {
        $class = null;
        $relationship = null;

        foreach ($this->query['models'] as $m) {
            foreach ($m::$relationships as $r) {
                if ($r['model'] == $model) {
                    $class = $m;
                    $relationship = $r;

                    break;
                }
            }
        }

        if ($relationship == null) {
            return $this;
        }

        if ($relationship['type'] == 'belongsTo') {
            $this->query['join'][] = sprintf(
                'LEFT JOIN `%s` ON `%s`.`id` = `%s`.`%s`',
                $model::$table, $model::$table, $class::$table, $relationship['fk']
            );
        } elseif ($relationship['type'] == 'hasOne') {
            $this->query['join'][] = sprintf(
                'LEFT JOIN `%s` ON `%s`.`%s` = `%s`.`id`',
                $model::$table, $model::$table, $relationship['fk'], $class::$table
            );
        } elseif ($relationship['type'] == 'hasMany') {
            $this->query['join'][] = sprintf(
                'LEFT JOIN `%s` ON `%s`.`%s` = `%s`.`id`',
                $model::$table, $model::$table, $relationship['fk'], $class::$table
            );
        } elseif ($relationship['type'] == 'hasManyThrough') {
            $through = $relationship['through'];

            $this->query['join'][] = sprintf(
                'LEFT JOIN `%s` ON `%s`.`%s` = `%s`.`id`',
                $through::$table, $through::$table, $relationship['fk'], $class::$table
            );

            $this->query['join'][] = sprintf(
                'LEFT JOIN `%s` ON `%s`.`id` = `%s`.`%s`',
                $model::$table, $model::$table, $through::$table, $relationship['through_fk']
            );
        }

        $this->query['models'][] = $model;

        return $this;
    }

    public function where($sql, $bindings = array())
    {
        $this->query['where'] = $sql;

        foreach ($bindings as $key => $value) {
            if (is_int($key)) {
                $this->query['bindings'][] = $value;
            } else {
                $this->query['bindings'][$key] = $value;
            }
        }

        return $this;
    }

    public function groupby($groupby)
    {
        $this->query['groupby'] = $groupby;

        return $this;
    }

    public function having($having)
    {
        $this->query['having'] = $having;

        return $this;
    }

    public function order($sql, $bindings = array())
    {
        $this->query['order'] = $sql;
        foreach ($bindings as $key => $value) {
            if (is_int($key)) {
                $this->query['bindings'][] = $value;
            } else {
                $this->query['bindings'][$key] = $value;
            }
        }

        return $this;
    }

    public function limit($sql, $bindings = array())
    {
        $this->query['limit'] = $sql;
        foreach ($bindings as $key => $value) {
            if (is_int($key)) {
                $this->query['bindings'][] = $value;
            } else {
                $this->query['bindings'][$key] = $value;
            }
        }

        return $this;
    }

    ///   ---   Methods   ---   ///


    public function debug()
    {
        if ($this->result === null) {
            $this->execute();
        }

        echo '<pre>';
        print_r($this->result);
        echo '</pre>';
    }

    public function result()
    {
        if ($this->result === null) {
            $this->execute();
        }

        return $this->result;
    }

    ///   ---   Iterator   ---   ///

    public function rewind()
    {
        if ($this->result === null) {
            $this->execute();
        }

        return reset($this->result);
    }

    public function next()
    {
        return next($this->result);
    }

    public function prev()
    {
        return prev($this->result);
    }

    public function valid()
    {
        return key($this->result) !== null;
    }

    public function current()
    {
        return current($this->result);
    }

    public function key()
    {
        return key($this->result);
    }

    ///   ---   arrayaccess   ---   ///

    public function offsetExists($offset)
    {
        return isset($this->result[$offset]);
    }

    public function offsetGet($offset)
    {
        return isset($this->result[$offset]) ? $this->result[$offset] : null;
    }

    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->result[] = $value;
        } else {
            $this->result[$offset] = $value;
        }
    }

    public function offsetUnset($offset)
    {
        unset($this->result[$offset]);
    }

    ///   ---   Countable   ---   ///

    public function count()
    {
        if ($this->result === null) {
            $this->execute();
        }

        return count($this->result);
    }
}
