<?php

namespace wilson\utilities;

/**
 * Transforme une chaine de caratère en url slug
 *
 * S'inspirer de https://github.com/cocur/slugify/blob/master/src/Slugify.php
 */
class Slugify
{
    /**
     * Transforme une chaine de caratère en url slug
     *
     * @param  string $str       chaine à transformer
     * @param  array  $replace   caratères speciaux à remplacer par le délimiteur
     * @param  string $delimiter délimiteur par defaut '-''
     * @return string            url slug
     */
    public function slugify($str, $replace = array(), $delimiter = '-')
    {
        $replace[] = "'";

        $str = str_replace($replace, ' ', $str);

        $clean = iconv('UTF-8', 'ASCII//TRANSLIT', $str);
        $clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $clean);
        $clean = strtolower(trim($clean, '-'));
        $clean = preg_replace("/[\/_|+ -]+/", $delimiter, $clean);

        return $clean;
    }
}
