<?php

namespace wilson\views;

class View
{
    /**
     * Contenu à afficher dans le layout
     * @var string
     */
    private $content;

    /**
     * Map contenant les helpers instanciers
     * @var array
     */
    private $helpers = array();

    /**
     * Path pour le layout
     * @var string
     */
    private $layout;

    /**
     * Chemin de la vue à rendre
     * @var string
     */
    private $template;

    /**
     * Variables pour la vue
     * @var array
     */
    private $viewVars = array();

    /**
     * Constructeur
     */
    public function __construct()
    {
    }

    /**
     * Récupere le helper en tant que propriété de la view
     *
     * @param  string $name Nom du helper
     * @return Helper       helper
     */
    public function __get($name)
    {
        if (! isset($this->helpers[$name])) {
            $caller = debug_backtrace();
            $message = sprintf(
                'Undefined property: %s::$%s File : %s Line : %s',
                $caller[0]['class'],
                $name,
                $caller[0]['file'],
                $caller[0]['line']
            );

            throw new \Exception($message, 1);
        }

        return $this->helpers[$name];
    }

    /**
     * Retourne le rendu
     * @return string
     */
    public function __toString()
    {
        return $this->render();
    }

    /**
     * Récupere le contenu du layout courant
     * @return string
     */
    public function content()
    {
        return $this->content;
    }

    /**
     * Inclus un element dans la vue
     * @param  string $path Chemin de l'élément
     * @param  array  $vars Variables à passer à l'élément, par défaut les variables de la vue
     * @return string       Rendu de l'élément
     */
    public function element($path, $vars = null)
    {
        if ($vars === null) {
            $vars = $this->viewVars;
        }

        try {
            ob_start();
            extract($vars);
            include($path);
            $html = ob_get_contents();
            ob_end_clean();
        } catch (\Exception $error) {
            ob_end_clean();
            throw $error;
        }

        return $html;
    }

    /**
     * Rendu de la vue selon le type de rendu désiré
     * @return string Rendu de la vue
     */
    public function render()
    {
        $currentLayout = $this->layout;
        $this->content = $this->element($this->template);

        while ($this->layout) {
            $layout = $this->layout;
            $this->layout = null;
            $this->content = $this->element($layout);
        }

        $this->layout = $currentLayout;

        return $this->content;
    }

    /**
     * Set des variables à passer au template
     * @param string $key   clé
     * @param mixed  $value variable
     */
    public function set($key, $value)
    {
        $this->viewVars[$key] = $value;
    }

    /**
     * Set un helper à la vue
     * @param string $name   Nom du helper
     * @param mixed  $helper Helper
     */
    public function setHelper($name, $helper)
    {
        $this->helpers[$name] = $helper;
    }

    /**
     * Set le layout du template courant
     * @param  string $layout path du layout
     */
    public function setLayout($layout)
    {
        $this->layout = $layout;
    }

    /**
     * Set le path du template à rendre
     * @param string $template path du template à rendre
     */
    public function setTemplate($template)
    {
        $this->template = $template;
    }
}
