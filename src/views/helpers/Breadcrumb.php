<?php

namespace wilson\views\helpers;

class Breadcrumb
{

    protected $request_url;
    protected $paths = array();
    protected $template;

    public function __construct($beforeBreadcrumb, $texte , $separator, $template ,$urlArray)
    {
        $this->template = $template;
        $this->request_url = $_SERVER['REQUEST_URI'];
        $this->beforeBreadcrumb = $beforeBreadcrumb;
        $this->separator = $separator;
        $this->paths[] = array(
            $texte,
            \wilson\Config::get('uri.root')
        );

        $url = strstr($this->request_url, '?', true);
        if (!$url) {
            $url = $this->request_url;
        }
        if ($url != '/') {
            // $url = substr($url, 0, 1) == '/' ? substr($url, 1) : $url;
            // foreach (explode('/', $url) as $key => $path) {
            //     if($urlArray[$key] != null ){
            //         $newPath = $urlArray[$key];
            //     }
            //     $this->paths[] = array(
            //         $this->relook($path),
            //         ($newPath) ? $this->paths[$key][1].'/'.$newPath : $this->paths[$key][1].'/'.$path
            //     );
            // }

            foreach ($urlArray as $url => $label) {
                $this->paths[] = array($label, '/'.$url);
            }
        }
    }

    public function relook($str)
    {
        return ucfirst(strtr(strtolower($str), '_-', '  '));
    }

    /*
     * Modifie les liens (valeurs) du bredcrumb :
     * - chaque value doit étre séparé par un points
     * - une valeur vide laisse le comportement par defaut
     * - une valeur à 'null' supprimer le lien
     */
    public function setGeneralSyntax($syntax)
    {
        $atoms = explode('.', $syntax);

        foreach ($atoms as $pos => $atom) {
            if ($atom !== 'null' && $atom !== '') {
                $this->setSyntax($pos, $atom);
            }
        }

        foreach ($atoms as $pos => $atom) {
            if ($atom === 'null') {
                $this->setSyntax($pos, null);
            }
        }
    }

    /*
     * Modifie un lien du breadcrumb :
     * - une value null supprime l'étape
     * - une value ou un href false laisse le comportement par défaut
     */
    public function setSyntax($pos, $value = false, $href = false)
    {
        if (count($this->paths) > $pos) {
            if ($value === null) {
                $this->paths = array_diff_key($this->paths, array($pos=>null));

            } elseif ($value && $href) {
                $this->paths[$pos] = array($value, $href);

            } elseif ($value) {
                $this->paths[$pos][0] = $value;

            } elseif ($href) {
                $this->paths[$pos][1] = $href;
            }
        }
    }

    public function getParams()
    {
        $current = array_pop($this->paths);
        return array(
            'beforeBreadcrumb' => $this->beforeBreadcrumb,
            'paths' => $this->paths,
            'separator' => $this->separator,
            'current' => $current
        );
    }

    public function display()
    {
        $current = array_pop($this->paths);
        include $this->template;
    }
}
