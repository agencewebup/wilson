<?php

namespace wilson\views\helpers;

class Html
{
    private $css = array();
    private $js = array();
    private $title;
    private $description;
    private $charset = 'UTF-8';
    private $canonical;
    private $prev;
    private $next;

    private $router;
    private $assetsBaseUrl;
    private $mediaBaseUrl;


    public function __construct($router, $assetsBaseUrl, $mediaBaseUrl)
    {
        $this->router = $router;
        $this->assetsBaseUrl = $assetsBaseUrl;
        $this->mediaBaseUrl = $mediaBaseUrl;
    }

    /**
     * Récupere une url depuis une route
     * @param  string|array $route    Le premier element du tableau est le shortcut.
     *                                La clé 'lang' pour la langue et 'prefix' pour le prefix.
     * @param  boolean $fullbase      Si true retourne l'url avec le nom de domaine
     * @return string  url
     */
    public function url($route, $fullbase = false)
    {
        return $this->router->url($route, $fullbase);
    }

    /**
     * Récupere l'url courrante
     * @param  string $lang           (Optionel) Retourne url dans la langue désirée
     * @param  boolean $fullbase      Si true retourne l'url avec le nom de domaine
     * @return string  url
     */
    public function currentUrl($lang = null, $fullbase = false)
    {
        $route = $this->info('route');
        if ($lang) {
            $route['lang'] = $lang;
        }

        return $this->url($route, $fullbase);
    }

    /**
     * Retourne les info du router concernant la page
     * @param  string $key (optionel) Si aucune récupere le tableau d'info sinon retourne une la valeur de la clé du tableau
     * @return mixed      Tableau d'info ou la valeur de sa clé
     */
    public function info($key = null)
    {
        $route = $this->router->currentRoute();

        if (array_key_exists($key, $route)) {
            return $route[$key];
        }

        return $route;
    }

    /**
     * Retourne url absolue pour les assets
     * @param  string $url url de l'asset
     * @return string
     */
    public function asset($url)
    {
        $asset = $url;

        if (! $url) {
            $asset = $this->assetsBaseUrl;
        } elseif ($url[0] == '/') {
            $asset = $this->assetsBaseUrl . $url;
        }

        return $asset;
    }

    /**
     * Retourne url absolue pour les media
     * @param  string $url url du media
     * @return string
     */
    public function media($url)
    {
        return $this->mediaBaseUrl . $url;
    }

    /**
     * Ajout d'un fichier css
     * @param string $url   Url du fichier css
     * @param string $block Nom du block à récupérer
     */
    public function addCss($url, $block = 'default')
    {
        if (! array_key_exists($block, $this->js)) {
            $this->js[$block] = array();
        }

        $this->css[$block] = $this->asset($url);
    }

    /**
     * Retourne les balises pour inclure le css
     * @return string html
     */
    public function css($block = 'default')
    {
        $html = "";
        foreach ($this->css as $url) {
            $html .= '<link rel="stylesheet" type="text/css" href="' . $url . "\">\n";
        }

        return $html;
    }

    /**
     * Ajout d'un fichier js
     * @param string $url   Url du fichier css
     * @param string $block Nom du block à récupérer
     */
    public function addJs($url, $block = 'default')
    {
        if (! array_key_exists($block, $this->js)) {
            $this->js[$block] = array();
        }

        $this->js[$block][] = $this->asset($url);
    }

    /**
     * Retourne les balises pour inclure le js
     * @param  string $block nom du block à récupérer
     * @return string        html
     */
    public function js($block = 'default')
    {
        if (! array_key_exists($block, $this->js)) {
            $this->js[$block] = array();
        }

        $html = "";
        foreach ($this->js[$block] as $url) {
            $html .= '<script type="text/javascript" src="' . $url . "\"></script>\n";
        }

        return $html;
    }

    /**
     * Set title
     * @param  string $title title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Retourne la balise title
     * @return string
     */
    public function title()
    {
        return '<title>' . $this->title . '</title>' . "\n";
    }

    /**
     * Set description
     * @param  string $description description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Retourne la balise meta pour la description
     * @return string
     */
    public function description()
    {
        return '<meta name="description" content="' . $this->description . '">' . "\n";
    }

    /**
     * Set charset
     * @param  string $charset charset
     */
    public function setCharset($charset)
    {
        $this->charset = $charset;
    }

    /**
     * Retourne la balise meta pour le charset
     * @return string
     */
    public function charset()
    {
        return '<meta charset="' . $this->charset . '" />' . "\n";
    }

    /**
     * Set canonical
     * @param  string $canonical url du lien canonique
     */
    public function setCanonical($canonical)
    {
        $this->canonical = $canonical;
    }


    /**
     * Retourne la balise meta pour le lien canonique s'il est set
     * @return string
     */
    public function canonical()
    {
        if ($this->canonical) {
            return '<link rel="canonical" href="' . $this->canonical . '" />' . "\n";
        }
        return "";
    }

    /**
     * Set la page précédente pour la pagination
     * @param  string $prev url de la page précédente
     */
    public function setPrev($prev)
    {
        $this->prev = $prev;
    }

    /**
     * Set la page suivante pour la pagination
     * @param  string $next url de la page suivante
     */
    public function setNext($next)
    {
        $this->next = $next;
    }

    /**
     * Retourne les balises pour les liens de pagination
     * @return string html
     */
    public function pagination()
    {
        $html = "";

        if ($this->prev) {
            $html .= '<link rel="prev" href="' . $this->prev . '" />' . "\n";
        }

        if ($this->next) {
            $html .= '<link rel="next" href="' . $this->next . '" />' . "\n";
        }

        return $html;
    }

    /**
     * Retourne la balise de hreflang
     * @return string html
     */
    public function hreflang()
    {
        $html = '';
        $languages = $this->router->languages();
        $currentRoute = $this->router->currentRoute();
        $route = $currentRoute['route'];

        foreach ($languages as $key => $value) {
            if ($currentRoute['lang'] != $key) {
                $route['lang'] = $key;
                $html .= '<link rel="alternate" hreflang="'.$key.'" href="'.$this->router->url($route, true).'">' . "\n";
            }
        }

        return $html;
    }
}
