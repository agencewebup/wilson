<?php

namespace wilson\views\helpers;


class Legal
{

    private $conf = array();
    private $template;

    public function legalNotice($template, $conf)
    {
        $this->template = $template;
        $this->conf = array_merge($this->conf, $conf);

        return $this->getLegalNotice();
    }

    private function getLegalNotice()
    {
        $content = @file_get_contents('http://git.webup.io/webup/mentions-legales/raw/master/' . $this->template);
        return $this->searchAndReplace($content);
    }

    private function searchAndReplace($content)
    {
        foreach($this->conf as $key => $value)
        {
            $content = str_replace('{{'. $key . '}}', $value, $content);
        }

        return $content;
    }

}
