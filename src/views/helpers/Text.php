<?php

namespace wilson\views\helpers;

class Text
{
    /**
     * Retourne le texte de la taille désiré (Et rajoute '...' si il est trop long)
     * @param  string $text Texte à rétrécir
     * @param  int    $size Taille maximum de la chaine voulu
     * @return string Texte rétréci si nécessaire
     */
    public function short($text, $size, $link = null)
    {
        if (strlen($text) > $size) {
            $text = substr($text, 0, strpos(wordwrap($text, $size), "\n")).' ...';
            if ($link) {
                $text .= " <a href='".$link."'>Lire la suite</a>";
            }
        }

        return $text;
    }

    /**
     * Retourne le texte nettoyé des balise HTML
     * @param  string $text Texte à nettoyer
     * @return string Texte nettoyé
     */
    public function strip($text)
    {
        return strip_tags($text);
    }

    /**
     * Retourne le texte nettoyé des balise HTML puis rétréci avec la taille désiré
     * @param  string $text Texte à rétrécir
     * @param  int    $size Taille maximum de la chaine voulu
     * @return string Texte nettoyé et rétréci
     */
    public function stripAndShort($text, $size)
    {
        return self::short(self::strip($text), $size);
    }

    /**
     * Retourne le lien nettoyer de 'http' et 'https'
     * @param  string             $url  Url à rétrécir
     * @param  int    (optionnel) $size Taille maximum de la chaine voulu
     * @return string             Url nettoyé et rétréci (si demandé)
     */
    public function prettyUrl($url, $size = 9999)
    {
        $url = str_replace('https://', '', str_replace('http://', '', $url));
        $string = (strlen($url) > $size) ? substr($url, 0, $size).'...' : $url;

        return $string;
    }
}
