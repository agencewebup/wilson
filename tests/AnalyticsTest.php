<?php

class AnalyticsTest extends PHPUnit_Framework_TestCase
{
    public function testTrackingCode()
    {
        $script = <<<EOT
<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-XXXX-Y', 'auto');
ga('send', 'pageview');

</script>

EOT;

        $analyticsHelper = new \wilson\views\helpers\Analytics();
        $this->assertEquals('', $analyticsHelper->trackingCode());

        $analyticsHelper->setTrackingCode('UA-XXXX-Y');
        $this->assertEquals($script, $analyticsHelper->trackingCode());
    }
}
