<?php

class HtmlTest extends PHPUnit_Framework_TestCase
{
    public $html;

    public function setUp()
    {
        $this->Html = new \wilson\views\helpers\Html(null, '/assets', '/media');
    }

    public function testTitle()
    {
        $this->Html->setTitle('My title');
        $this->assertEquals('My title', $this->Html->getTitle());
        $this->assertEquals('<title>My title</title>'."\n", $this->Html->title());
    }

    public function testDescription()
    {
        $this->Html->setDescription('My description');
        $this->assertEquals('My description', $this->Html->getDescription());
        $this->assertEquals('<meta name="description" content="My description">'."\n", $this->Html->description());
    }

    public function testHreflang()
    {
        $router = new \wilson\Router();

        $router->setBaseUrl('http://www.example.com');

        $router->setLanguages(array(
            'fr' => 'fr_FR.UTF-8',
            'en' => 'en_US.UTF-8',
            'de-es' => 'de_ES.UTF-8'
        ));

        $router->addI18n('home', '', '\namespace\Controller.home');

        $router->setCurrentRoute(array(
            'route' => array(
                'home',
                'prefix' => 'default',
                'lang' => 'fr',
            ),
            'shortcut' => 'home',
            'controller' => '\namespace\Controller',
            'action' => 'home',
            'args' => array(),
            'prefix' => 'default',
            'lang' => 'fr',
            'controllerName' => 'Controller'
        ));


        $Html = new \wilson\views\helpers\Html($router, '/assets', '/media');

        $hreflang = <<<HTML
<link rel="alternate" hreflang="en" href="http://www.example.com/en">
<link rel="alternate" hreflang="de-es" href="http://www.example.com/de-es">

HTML;

        $this->assertEquals($hreflang, $Html->hreflang());
    }
}
