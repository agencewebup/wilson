<?php

use wilson\log\Log;

class LoggerTest extends PHPUnit_Framework_TestCase
{
    public $filename;

    public function setUp()
    {
        $this->filename = 'tests/tmp/error.log';
        if (file_exists($this->filename)) {
            unlink($this->filename);
        }
    }

    public function testLog()
    {
        $logger = new \wilson\log\FileLogger($this->filename);

        $this->assertEquals(0, count(Log::getLoggers()));

        Log::addLogger($logger);

        $this->assertEquals(1, count(Log::getLoggers()));

        $message = 'emergency message';
        Log::emergency($message);
        $contents = file_get_contents($this->filename);
        $this->assertContains('Emergency: '.$message, $contents);

        $message = 'alert message';
        Log::alert($message);
        $contents = file_get_contents($this->filename);
        $this->assertContains('Alert: '.$message, $contents);

        $message = 'critical message';
        Log::critical($message);
        $contents = file_get_contents($this->filename);
        $this->assertContains('Critical: '.$message, $contents);

        $message = 'error message';
        Log::error($message);
        $contents = file_get_contents($this->filename);
        $this->assertContains('Error: '.$message, $contents);

        $message = 'warning message';
        Log::warning($message);
        $contents = file_get_contents($this->filename);
        $this->assertContains('Warning: '.$message, $contents);

        $message = 'notice message';
        Log::notice($message);
        $contents = file_get_contents($this->filename);
        $this->assertContains('Notice: '.$message, $contents);

        $message = 'info message';
        Log::info($message);
        $contents = file_get_contents($this->filename);
        $this->assertContains('Info: '.$message, $contents);

        $message = 'debug message';
        Log::debug($message);
        $contents = file_get_contents($this->filename);
        $this->assertContains('Debug: '.$message, $contents);

        Log::removeLogger($logger);
        $this->assertEquals(0, count(Log::getLoggers()));
    }
}
