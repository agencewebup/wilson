<?php

use wilson\Router;

class RouterTest extends PHPUnit_Framework_TestCase
{
    private $router;

    public function setUp()
    {
        $this->router = new \wilson\Router();

        $this->router->setBaseUrl('http://example.com');

        $this->router->setLanguages(array(
            'fr' => 'fr_FR.UTF-8',
            'en' => 'en_US.UTF-8'
        ));

        $this->router->setDefaultLang('fr');

        $this->router->add('simple', '/simple', '\namespace\Controller.simple');
        $this->router->add('args', '/args/(?P<id>\d+)/(?P<title>.*)', '\namespace\Controller.args');
        $this->router->addI18n('lang', '/lang', '\namespace\Controller.lang');

        $this->router->startPrefix('prefix');
        $this->router->add('simple', '/simple', '\namespace\Controller.prefix');
        $this->router->addI18n('prefixlang', '/prefixlang', '\namespace\Controller.prefixlang');
        $this->router->addI18n('prefixlangs', array(
            'fr' => '/url-en-francais',
            'en' => '/english-url'
        ), '\namespace\Controller.prefixlangs');
        $this->router->setExceptionHandler('\namespace\PrefixError');
        $this->router->endPrefix();
    }

    public function testRoute()
    {
        $this->assertEquals(array(
            'route' => array(
                'args', 'id' => '404',
                'title'=>'not-found',
                'prefix' => 'default',
                'lang' => 'fr',
            ),
            'shortcut' => 'args',
            'controller' => '\namespace\Controller',
            'action' => 'args',
            'args' => array('id' => '404', 'title'=>'not-found'),
            'prefix' => 'default',
            'lang' => 'fr',
            'controllerName' => 'Controller'
        ), $this->router->route('/args/404/not-found'));

        $this->assertEquals(array(
            'route' => array(
                'lang',
                'prefix' => 'default',
                'lang' => 'en',
            ),
            'shortcut' => 'lang',
            'controller' => '\namespace\Controller',
            'action' => 'lang',
            'args' => array(),
            'prefix' => 'default',
            'lang' => 'en',
            'controllerName' => 'Controller'
        ), $this->router->route('/en/lang'));

        $this->assertEquals(array(
            'route' => array(
                'prefixlangs',
                'prefix' => 'prefix',
                'lang' => 'en',
            ),
            'shortcut' => 'prefixlangs',
            'controller' => '\namespace\Controller',
            'action' => 'prefixlangs',
            'args' => array(),
            'prefix' => 'prefix',
            'lang' => 'en',
            'controllerName' => 'Controller'
        ), $this->router->route('/en/prefix/english-url'));

        $this->assertEquals(array(
            'prefix' => 'prefix',
            'lang' => 'fr',
        ), $this->router->route('/prefix/fail'));
    }

    public function testUrl()
    {
        $this->assertEquals('/simple', $this->router->url(array('simple')));
        $this->assertEquals('http://example.com/simple', $this->router->url(array('simple'), true));
        $this->assertEquals('/args/42/the-answer', $this->router->url(array('args', 'id'=>42, 'title'=>'the-answer')));
        $this->assertEquals('/args//the-answer', $this->router->url(array('args', 'title'=>'the-answer')));
        $this->assertEquals('/prefix/simple', $this->router->url(array('simple', 'prefix'=>'prefix')));
        $this->assertEquals('/fr/lang', $this->router->url(array('lang')));
        $this->assertEquals('/en/lang', $this->router->url(array('lang', 'lang'=>'en')));
        $this->assertEquals('/en/lang', $this->router->url(array('lang', 'lang'=>'en', 'fake'=>'fake')));

        $this->router->setPrefix('prefix');
        $this->assertEquals('/prefix/simple', $this->router->url(array('simple')));
        $this->router->setPrefix('default');

        $this->router->setLang('en');
        $this->assertEquals('fr', $this->router->defaultLang());
        $this->assertEquals('/en/lang', $this->router->url(array('lang')));
        $this->router->setLang(null);
    }

    public function testExceptionHandler()
    {
        $this->router->setPrefix('prefix');
        $this->assertEquals('\namespace\PrefixError', $this->router->exceptionHandler());
        $this->router->setPrefix('default');
    }

    public function testSymmetric()
    {
        $this->assertEquals(
            '/args/404/not-found',
            $this->router->url($this->router->route('/args/404/not-found')['route'])
        );

        $this->assertEquals(
            '/en/prefix/english-url',
            $this->router->url($this->router->route('/en/prefix/english-url')['route'])
        );
    }
}
