<?php

class SlugifyTest extends PHPUnit_Framework_TestCase
{
    public function testSlugify()
    {
        $tests = array(
            array(' a  b ', 'a-b'),
            array('Hello', 'hello'),
            array('Hello World', 'hello-world'),
            array('Привет мир', 'privet-mir'),
            array('Привіт світ', 'privit-svit'),
            array('Hello: World', 'hello-world'),
            array('H+e#l1l--o/W§o r.l:d)', 'h-e-l1l-o-w-o-r-l-d'),
            array(': World', 'world'),
            array('Hello World!', 'hello-world'),
            array('Ä ä Ö ö Ü ü ß', 'ae-ae-oe-oe-ue-ue-ss'),
            array('Á À á à É È é è Ó Ò ó ò Ñ ñ Ú Ù ú ù', 'a-a-a-a-e-e-e-e-o-o-o-o-n-n-u-u-u-u'),
            array('Â â Ê ê Ô ô Û û', 'a-a-e-e-o-o-u-u'),
            array('Â â Ê ê Ô ô Û 1', 'a-a-e-e-o-o-u-1'),
            array('°¹²³@', '0123at'),
            array('Mórë thån wørds', 'more-than-words'),
            array('Блоґ їжачка', 'blog-jizhachka'),
            array('фильм', 'film'),
            array('драма', 'drama'),
            array('ελληνικά', 'ellenika'),
            array('C’est du français !', 'c-est-du-francais'),
            array('هذه هي اللغة العربية', 'hthh-hy-llgh-laarby'),
            array('مرحبا العالم', 'mrhb-laa-lm'),
            array('Één jaar', 'een-jaar'),
            array('tiếng việt rất khó', 'tieng-viet-rat-kho')
        );

        $slugify = new \wilson\utilities\Slugify();

        foreach ($tests as $test) {
            $this->assertEquals($test[1], $slugify->slugify($test[0]));
        }
    }
}
