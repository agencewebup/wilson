<?php

class TextTest extends PHPUnit_Framework_TestCase
{
    public $class;
    public $text;

    public function setUp()
    {
        $this->class = new \wilson\views\helpers\Text();
        $this->text = 'Lorem ipsum Id <img src="http://www.google.fr"/>Excepteur ut <a>fugiat</a> irure reprehenderit laborum nisi occaecat eiusmod mollit ex enim';
    }

    public function testShort()
    {
        $this->assertEquals('Lorem ipsum Id <img src="http://www.google.fr"/>Excepteu...', $this->class->short($this->text, 56));
    }

    public function testStrip()
    {
        $this->assertEquals('Lorem ipsum Id Excepteur ut fugiat irure reprehenderit laborum nisi occaecat eiusmod mollit ex enim', $this->class->strip($this->text));
    }

    public function testStripAndShort()
    {
        $this->assertEquals('Lorem ipsum Id Excepteur ut fugiat irure rep...', $this->class->stripAndShort($this->text, 44));
    }

    public function testPrettyUrl()
    {
        $url = 'http://wordpress.brybry.fr';
        $this->assertEquals('wordpress.brybry.fr', $this->class->prettyUrl($url));

        $url = 'https://wordpress.brybry.fr';
        $this->assertEquals('wordpress.brybry.fr', $this->class->prettyUrl($url));

        $url = 'http://wordpress.brybry.fr';
        $this->assertEquals('wordpress.brybry...', $this->class->prettyUrl($url, 16));
    }
}
