<?php

class ViewTest extends PHPUnit_Framework_TestCase
{
    /**
     * Path des fixtures
     * @var string
     */
    public $path;

    public function setUp()
    {
        $this->path = dirname(__FILE__);
    }

    public function testRender()
    {
        $view = new \wilson\views\View();
        $view->setTemplate($this->path . '/fixtures/content.tpl');
        $this->assertEquals('content', $view->render());
    }

    public function testLayout()
    {
        $view = new \wilson\views\View();
        $view->setTemplate($this->path . '/fixtures/content.tpl');
        $view->setLayout($this->path . '/fixtures/layout.tpl');
        $this->assertEquals('header content footer', $view->render());
    }

    public function testVar()
    {
        $view = new \wilson\views\View();
        $view->setTemplate($this->path . '/fixtures/var.tpl');
        $view->set('foo', 'bar');
        $this->assertEquals('bar', $view->render());
    }

    public function testHelper()
    {
        $view = new \wilson\views\View();
        $view->setHelper('foo', 'bar');
        $this->assertEquals('bar', $view->foo);

        $this->setExpectedExceptionRegExp('Exception', '/Undefined property.*/');
        $view->fail;
    }

    public function testToString()
    {
        $view = new \wilson\views\View();
        $view->setTemplate($this->path . '/fixtures/content.tpl');
        $this->assertEquals('content', $view);
    }
}
